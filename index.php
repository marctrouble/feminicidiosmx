<!DOCTYPE html>
<html>

<head>
<link rel="icon" type="image/x-icon" href="images/ico.ico">
  <!-- Basic -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <!-- Site Metas -->
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <meta charset="utf-8">
  <title>Feminicidios en México 2016-2020</title>
  <!-- slider stylesheet -->
  <link rel="stylesheet" type="text/css"
    href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css" />
  <!-- bootstrap core css -->
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
  <!-- fonts style -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:400,700|Roboto:400,700&display=swap" rel="stylesheet">
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet" />
  <!-- responsive style -->
  <link href="css/responsive.css" rel="stylesheet" />
  <!-- mis estilos -->
  <link rel="stylesheet" href="https://cdn.plot.ly/plotly-latest.min.css">
  <link rel="stylesheet" href="css/styles.css">
  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
  <script src="js/graficas.js"></script>
</head>

<body>

  <header>
	
  </header>

  <div class="hero_area">
    <!-- header section strats -->
    <header class="header_section">
      <div class="container-fluid">
        <nav class="navbar navbar-expand-lg custom_nav-container ">
        <a href="inicio.php" class="btn btn-danger" style="margin-right: 20px;"><span class="glyphicon glyphicon-chevron-left"></span>Inicio</a>
          
        <a class="navbar-brand" href="">
          <div align="center">
            <span>
              Feminicidios en México 2016-2020
            </span>
          
          </div>
        </a>
          <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <div class="d-flex mx-auto flex-column flex-lg-row align-items-center">
              <ul class="navbar-nav  ">
                <li class="nav-item active">
                  <a class="nav-link" href=#8m>8M <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href=#ni> Ni una Menos </a>
                </li>
              </ul>
            </div>
          </div>
          <form class="form-inline my-2 my-lg-0 ml-0 ml-lg-4 mb-3 mb-lg-0">
            <button class="btn  my-2 my-sm-0 nav_search-btn" type="submit"></button>
          </form>
        </nav>
      </div>
    </header>
    <!-- end header section -->
   
  <div class="bg">
    <!-- about section -->
    <section class="about_section layout_padding">
      <div class="container">
        <div class="custom_heading-container">
          <h3 class=" ">
          Estadísticas por estado
          </h3>
        </div>
        </section>

       <main>
  <div class="combo-container">
    <label for="combo">Selecciona un estado:</label>
    <div class="combo-box">
      <select id="combo" name="estado">
        <option value="" disabled selected>Elige un estado</option>
        <?php
          include 'conexion.php';
          $sql = "SELECT nombre FROM estados";
          $result = $conn->query($sql);
          while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            echo "<option value='" . $row['nombre'] . "'>" . $row['nombre'] . "</option>";
          }
        ?>
      </select>
      <i class="fas fa-chevron-down"></i>
    </div>
  </div>
  <div id="mapa-container" class="mapa-container">
   <iframe id="mapa-iframe" width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="" style="border: 1px solid black"></iframe>
      <br/>
      <small><a id="mapa-link" href="">Ver mapa más grande</a></small>
  <div class="graficas-container">
    
    <div id="grafica-lineal" class="grafica"></div>
    <div id="grafica-barras" class="grafica"></div>
   
    </div>
  </div>
</main>

<script>
  $(document).ready(function() {
    $("#combo").change(function() {
      var estado = $(this).val();
      $.post("actualizar_graficas.php", {
        estado: estado
      }, function(data) {
        var graficas = JSON.parse(data);
        var mapa = graficas.pop();
        Plotly.newPlot('grafica-lineal', graficas[0].data, graficas[0].layout);
        Plotly.newPlot('grafica-barras', graficas[1].data, graficas[1].layout);
        $("#mapa-iframe").attr("src", mapa);
        $("#mapa-link").attr("href", mapa);
      });
    });
  });
</script>
    <!-- end about section -->

    <!-- service section -->

    <section class="service_section layout_padding-bottom">
      <div class="container">
        <div class="custom_heading-container">
          <h3 class=" "id='8m'>
          #8M #NiUnaMenos
          </h3>
        </div>
        <p class="">
        Si mañana acudes a las movilizaciones en tu localidad por el #8Marzo2022 💜, te compartimos unas recomendaciones para el autocuidado.🤜✨🤛
        </p>
        <div class="service_container ">
          <div class="row">
            <div class="col-md-3">
              <div class="box b-1">
                <div class="img-box">
                  <img src="images/s-1.jpg" alt="">
                </div>
                <div class="detail-box">
                  <h6>
                  Recomendaciones marcha 8M: claves para antes, durante y después
                  </h6>
                  <p>
                  Es por ello que para marchar hay una serie de medidas y 
                  sugerencias que se dan por arte de colectivos feministas y 
                  organizaciones civiles útiles en todo momento de la concentración.
                  </p>
                  <div class="btn-box">
                   
                    <hr>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="box b-2">
                <div class="img-box">
                  <img src="images/s-2.jpg" alt="">
                </div>
                <div class="detail-box">
                  <h6>
                   
                  </h6>
                  <p>
                 
                  </p>
                  <div class="btn-box">
                    <a href="">
                      
                    </a>
                    <hr>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="box b-3">
                <div class="img-box">
                  <img src="images/s-3.jpg" alt="">
                </div>
                <div class="detail-box">
                  <h6>
                   
                  </h6>
                  <p>
                    
                  </p>
                  <div class="btn-box">
                    <a href="">
                     
                    </a>
                    <hr>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="box b-4">
                <div class="img-box">
                  <img src="images/s-4.jpg" alt="">
                </div>
                <div class="detail-box">
                  <h6>
                    
                  </h6>
                  <p>
                   
                  </p>
                  <div class="btn-box">
                    <a href="">
                     
                    </a>
                    <hr>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- end service section -->

    <!-- work section -->

    <section class="work_section layout_padding">
      <div class="container">
        <div class="custom_heading-container">
          <h3 class=" "id='ni'>
          Ángulos y rutas de investigación en la cobertura de feminicidios
          </h3>
        </div>
      </div>
      <div class="work_container ">
        <div class="box b-1">
          <div class="img-box">
            <img src="images/w-1.png" alt="">
          </div>
          <div class="name">
            <h6>
              1
            </h6>
          </div>
        </div>
        <div class="box b-2">
          <div class="img-box ">
            <img src="images/w-2.png" alt="">
          </div>
          <div class="name">
            <h6>
              2
            </h6>
          </div>
        </div>
        <div class="box b-3">
          <div class="img-box ">
            <img src="images/w-3.png" alt="">
          </div>
          <div class="name">
            <h6>
              3
            </h6>
          </div>
        </div>
        <div class="box b-4">
          <div class="img-box ">
            <img src="images/w-4.png" alt="">
          </div>
          <div class="name">
            <h6>
              4
            </h6>
          </div>
        </div>
      </div>
    </section>


    <!-- end work section -->


    
    <section class="info_section layout_padding">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
          </div>
          <div class="col-md-3">
            <div class="info-contact">
              <h4>
                Necesitas ayuda?
              </h4>
              <div class="location">
                <h6>
                   Dirección:
                </h6>
                <a href="">
                  <img src="images/location.png" alt="">
                  <span>
                  Delegación del Gobierno contra la Violencia de Género
                  </span>
                </a>
              </div>
              <div class="call">
                <h6>
                  ¡Llamanos!
                </h6>
                <a href="">
                  <img src="images/telephone.png" alt="">
                  <span>
                    ( +01 1234567890 )
                  </span>
                </a>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="discover">
              <h4>
                Somos Más
              </h4>
              <ul>
                <li>
                  <a href="">
                    Ayuda
                  </a>
                </li>
                <li>
                  <a href="">
                    Como funciona esta organización

                  </a>
                </li>
                <li>
                  <a href="">
                    Registro
                  </a>
                </li>
                <li>
                  <a>
                    Buscanos en redes sociales
                  </a>
                </li>
              </ul>
              <div class="social-box">
                <a href="">
                  <img src="images/facebook.png" alt="">
                </a>
                <a href="">
                  <img src="images/twitter.png" alt="">
                </a>
                <a href="">
                  <img src="images/google-plus.png" alt="">
                </a>
                <a href="">
                  <img src="images/linkedin.png" alt="">
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- end info_section -->
    <!-- footer section -->
    <section class="container-fluid footer_section">
      <p>
        Copyright &copy; 2023 All Rights Reserved By Marco Antonio Ortega  Rojas
      </p>
    </section>
    <!-- footer section -->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
</html>
</body>
</html>

