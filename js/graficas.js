$(document).ready(function() {
  // Llenar el combo box con los nombres de los estados
  $.ajax({
    url: 'combo_box.php',
    type: 'POST',
    success: function(data) {
      $('#combo').html(data);
    },
  });

  // Función para actualizar las gráficas
  function actualizarGraficas(estado) {
    
    $.ajax({
      url: 'actualizar_graficas.php',
      type: 'POST',
      data: {estado: estado},
      success: function(data) {
        // Convertir la respuesta JSON en objetos de Plotly
        var graficas = JSON.parse(data);

        // Mostrar las gráficas de línea y de barras
        Plotly.newPlot('grafica-lineal', graficas[0].data, graficas[0].layout);
        Plotly.newPlot('grafica-barras', graficas[1].data, graficas[1].layout);
      },
      error: function() {
        alert('Error al actualizar las gráficas');
      }
      
    });
  }

  // Actualizar las gráficas cuando se selecciona un estado en el combo box
  $('#combo').on('change', function() {
    var estado = $(this).val();
    if (estado) {
      actualizarGraficas(estado);
    }
  });

  // Actualizar las gráficas cuando se carga la página (para el estado seleccionado en el combo box)
  var estadoInicial = $('#combo').val();
  if (estadoInicial) {
    actualizarGraficas(estadoInicial);
  }
});
