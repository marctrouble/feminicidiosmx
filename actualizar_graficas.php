<?php
include 'conexion.php';

if (isset($_POST['estado'])) {
  $estado = $_POST['estado'];

  // Obtener los datos de la base de datos
  $sql = "SELECT fem_2016, fem_2017, fem_2018, fem_2019, fem_2020, ubicacion FROM estados WHERE nombre = ?";
  $stmt = $conn->prepare($sql);
  $stmt->execute([$estado]);
  $row = $stmt->fetch(PDO::FETCH_ASSOC);

  // Crear la gráfica de línea
  $data_lineal = array(
    'x' => array('2016', '2017', '2018', '2019', '2020'),
    'y' => array($row['fem_2016'], $row['fem_2017'], $row['fem_2018'], $row['fem_2019'], $row['fem_2020']),
    'type' => 'scatter',
    'name' => 'Feminicidios'
  );
  $layout_lineal = array(
    'title' => 'Feminicidios en ' . $estado . ' (2016-2020)',
    'xaxis' => array('title' => 'Año'),
    'yaxis' => array('title' => 'Número de feminicidios')
  );
  $grafica_lineal = array('data' => array($data_lineal), 'layout' => $layout_lineal);

  // Crear la gráfica de barras
  $data_barras = array(
    'x' => array('2016', '2017', '2018', '2019', '2020'),
    'y' => array($row['fem_2016'], $row['fem_2017'], $row['fem_2018'], $row['fem_2019'], $row['fem_2020']),
    'type' => 'bar',
    'name' => 'Feminicidios'
  );
  $layout_barras = array(
    'title' => 'Feminicidios en ' . $estado . ' (2016-2020)',
    'xaxis' => array('title' => 'Año'),
    'yaxis' => array('title' => 'Número de feminicidios')
  );
  $grafica_barras = array('data' => array($data_barras), 'layout' => $layout_barras);

  // Imprimir las gráficas y el mapa
  echo json_encode(array($grafica_lineal, $grafica_barras, $row['ubicacion']));
}
?>
