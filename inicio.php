<!DOCTYPE html>
<html>
<head>
<link rel="icon" type="image/x-icon" href="images/ico.ico">
	<meta charset="utf-8">
	<title>Feminicidios en México</title>
	<link rel="stylesheet" href="css/inicio.css">
</head>
<body>

	<table>
		<tr>
			<td class="dark">
				<h1>Sólo en México...</h1>
				<p>Cada día 10 mujeres son asesinadas.</p>
			</td>
			<td class="dark">
				<h1>Los feminicidas...</h1>
				<p>La gran mayoría no recibe castigo alguno.</p>
			</td>
			<td class="dark">
				<h1>El Estado...</h1>
				<p>No garantiza la seguridad de las mujeres.</p>
			</td>
		</tr>
		<tr>
			<td class="dark">
				<h1>Las cifras...</h1>
				<p>Son alarmantes: en 2020 se registraron 939 feminicidios.</p>
			</td>
			<td class="light" id="imagen">
				<a href="index.php"><img src="images/centro.png" alt="Feminicidios en México"></a>
			</td>
			<td class="dark">
				<h1>La justicia...</h1>
				<p>No llega para las víctimas y sus familias.</p>
			</td>
		</tr>
		<tr>
			<td class="dark">
				<h1>La prevención...</h1>
				<p>Es fundamental para erradicar la violencia contra las mujeres.</p>
			</td>
			<td class="dark">
				<h1>La lucha...</h1>
				<p>No terminará hasta que se haga justicia.</p>
			</td>
			<td class="dark">
				<h1>Las voces...</h1>
				<p>De las mujeres y de quienes las apoyan, deben ser escuchadas.</p>
			</td>
		</tr>
	</table>

</body>
</html>
